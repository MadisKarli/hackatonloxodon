package com.ut.hackaton.demo.helpers;

/**
 * Created by Joonas Papoonas on 12/11/2017.
 */
//LOMBOK PLZ
public class Tuple {
    private static String first;
    private static String second;
    private static String hiddenThird;

    public Tuple(String first, String second){
        this.first = first;
        this.second = second;
    }

    public String getFirst(){
        return this.first;
    }

    public String getSecond(){
        return this.second;
    }

    public void setHiddenThird(String t){
        this.hiddenThird = t;
    }

}

package com.ut.hackaton.demo.helpers;

/**
 * Created by Madis-Karli Koppel on 12/11/2017.
 */

import org.bytedeco.javacv.*;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameGrabber.Exception;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class FrameExtractor {


    public static ArrayList<String> extract(String filename) throws IOException, Exception {

        Java2DFrameConverter converter = new org.bytedeco.javacv.Java2DFrameConverter();

        ArrayList<String> frames = new ArrayList();


        FFmpegFrameGrabber g = new FFmpegFrameGrabber(filename);
        g.start();

        // We don't know how many images there are so there's a small hack
        try {
            for (int i = 0; i < 500; i++) {

                // save only every n'th frame
                if(i % 14 != 0){
                    g.grabImage();
                    continue;
                }

                // TODO make images smaller
                BufferedImage f = converter.convert(g.grabImage());
                // BufferedImage f = rotateImageByDegrees(converter.convert(g.grabImage()), 90);

                String name = "C:\\hackaton\\frame-dump\\video-frame-" + System.currentTimeMillis() + ".png";
                frames.add(name);
                ImageIO.write(f, "png", new File(name));
            }
            g.stop();
        } catch (IllegalArgumentException e){
            // when there are no more files
            g.stop();
        } catch (NullPointerException e){
            g.stop();
        }

        return frames;

    }


    public static BufferedImage rotateImageByDegrees(BufferedImage img, double angle) {

        double rads = Math.toRadians(angle);
        double sin = Math.abs(Math.sin(rads)), cos = Math.abs(Math.cos(rads));
        int w = img.getWidth();
        int h = img.getHeight();
        int newWidth = (int) Math.floor(w * cos + h * sin);
        int newHeight = (int) Math.floor(h * cos + w * sin);

        BufferedImage rotated = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = rotated.createGraphics();
        AffineTransform at = new AffineTransform();
        at.translate((newWidth - w) / 2, (newHeight - h) / 2);

        int x = w / 2;
        int y = h / 2;

        at.rotate(rads, x, y);
        g2d.setTransform(at);
        g2d.drawImage(img, 0, 0, null);
        g2d.setColor(Color.RED);
        g2d.drawRect(0, 0, newWidth - 1, newHeight - 1);
        g2d.dispose();

        return rotated;
    }


}
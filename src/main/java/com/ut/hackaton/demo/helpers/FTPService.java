package com.ut.hackaton.demo.helpers;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import org.apache.commons.net.ftp.FTPClient;

import javax.validation.constraints.Null;
import java.io.FileInputStream;
import java.io.File;
import java.io.IOException;

/**
 * Created by Madis-Karli Koppel on 12/11/2017.
 */
public class FTPService {

    public static String send(String fileName) {
        String SFTPHOST = "math.ut.ee";
        int SFTPPORT = 22;
        String SFTPUSER = "matukopp";
        String SFTPPASS = "plzdonthackme";
        // also change the return part
        // as the method returns an URL
        String SFTPWORKINGDIR = "public_html/ftptest";

        Session session = null;
        Channel channel = null;
        ChannelSftp channelSftp = null;
        System.out.println("preparing the host information for sftp.");
        try {
            JSch jsch = new JSch();
            session = jsch.getSession(SFTPUSER, SFTPHOST, SFTPPORT);
            session.setPassword(SFTPPASS);
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();
            //System.out.println("Host connected.");
            channel = session.openChannel("sftp");
            channel.connect();
            //System.out.println("sftp channel opened and connected.");
            channelSftp = (ChannelSftp) channel;
            channelSftp.cd(SFTPWORKINGDIR);
            //System.out.println("prepare file upload");
            File f = new File(fileName);
            channelSftp.put(new FileInputStream(f), f.getName());
            System.out.println("File transfered successfully to host.");
            //TODO remove hardcodes
            return "http://kodu.ut.ee/~matukopp/ftptest/" + f.getName();
        } catch (Exception ex) {
            System.out.println("Exception found while tranfer the response.");
            return "FAIL";
        }
        finally{
            channelSftp.exit();
            //System.out.println("sftp Channel exited.");
            channel.disconnect();
            //System.out.println("Channel disconnected.");
            session.disconnect();
            System.out.println("Host Session disconnected.");
        }
    }

}

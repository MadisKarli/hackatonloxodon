package com.ut.hackaton.demo.service;

import com.ut.hackaton.demo.helpers.FTPService;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64.Encoder;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

import static org.springframework.util.Base64Utils.encodeToString;

/**
 * Created by Madis-Karli Koppel on 11/11/2017.
 */
@Service
public class DetectionService {
    private static final String api = "https://dev.sighthoundapi.com/v1/recognition?objectType=vehicle,licenseplate";
    private static final String accessToken = "sfOIFnb6YuV0T1gsV0jfd6UpTTe8B08bPFYZ";


    public String getimage(String url, String contentType) throws IOException {
        JsonObject jsonImage = Json.createObjectBuilder().add("image", url).build();
        // JsonObject jsonImage = Json.createObjectBuilder().add("image", url).build();
        URL apiURL = new URL(api);
        HttpURLConnection connection = (HttpURLConnection) apiURL.openConnection();
        connection.setRequestProperty("Content-Type", contentType);
        connection.setRequestProperty("X-Access-Token", accessToken);
        connection.setRequestMethod("POST");
        connection.setDoInput(true);
        connection.setDoOutput(true);
        byte[] body = jsonImage.toString().getBytes();
        connection.setFixedLengthStreamingMode(body.length);
        OutputStream os = connection.getOutputStream();
        os.write(body);
        os.flush();
        int httpCode = connection.getResponseCode();
        if ( httpCode == 200 ){
            JsonReader jReader = Json.createReader(connection.getInputStream());
            JsonObject jsonBody = jReader.readObject();
            //System.out.println(jsonBody);
            return jsonBody.toString();

        } else {
            JsonReader jReader = Json.createReader(connection.getErrorStream());
            JsonObject jsonError = jReader.readObject();
            //System.out.println(jsonError);
            return jsonError.toString();

        }
    }

    public String encode(byte[] bytes) throws IOException {
        String stuff = encodeToString(bytes);
        Base64Utils.encode(bytes);
        // System.out.println(stuff);
        return getimage(stuff, "application/octet-stream");
        // or "application/json"
    }

    public String uploadToDFTP(String filename){
        String res = FTPService.send(filename);
        if(!res.equals("FAIL")){
            //TODO handle
        }

        return res;
    }
}

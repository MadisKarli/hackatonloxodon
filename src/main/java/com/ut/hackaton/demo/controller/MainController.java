package com.ut.hackaton.demo.controller;

import com.ut.hackaton.demo.helpers.FrameExtractor;
import com.ut.hackaton.demo.service.DetectionService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartResolver;

import javax.servlet.MultipartConfigElement;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Madis-Karli Koppel on 11/11/2017.
 */

//SORRY FOR CROSSORIGIN
@CrossOrigin
@Controller
@RestController
public class MainController {

    @Autowired
    DetectionService detectionService;

    private final String filepath = "C:/hackaton/"; //saved uploads

    @RequestMapping("/help")
    public String help(@RequestParam("url") String url) {
        try {
            return detectionService.getimage(url, "application/json");
        } catch (IOException e) {
            e.printStackTrace();
        }
        // TODO return correct error
        return "500";
    }

    @Bean
    public MultipartConfigElement multipartConfigElement() {
        return new MultipartConfigElement("");
    }

    @Bean
    public MultipartResolver multipartResolver() {
        org.springframework.web.multipart.commons.CommonsMultipartResolver multipartResolver = new org.springframework.web.multipart.commons.CommonsMultipartResolver();
        multipartResolver.setMaxUploadSize(200447940);
        return multipartResolver;
    }


    @RequestMapping(value = "uploaddata", method = RequestMethod.POST)
    public String uploadDataFile(
            @RequestParam(value = "file", required = true) MultipartFile file) {
        // return "{\"results\":[{\"image\":\"http://kodu.ut.ee/~matukopp/ftptest/video-frame-1510452907663.png\",\"car\":\"Infiniti\",\"confidence\":\"0.5757\"},{\"image\":\"http://kodu.ut.ee/~matukopp/ftptest/video-frame-1510452908181.png\",\"car\":\"Pontiac\",\"confidence\":\"0.4427\"},{\"image\":\"http://kodu.ut.ee/~matukopp/ftptest/video-frame-1510452908667.png\",\"car\":\"Saturn\",\"confidence\":\"0.1486\"},{\"image\":\"http://kodu.ut.ee/~matukopp/ftptest/video-frame-1510452909217.png\",\"car\":\"Saturn\",\"confidence\":\"0.5821\"},{\"image\":\"http://kodu.ut.ee/~matukopp/ftptest/video-frame-1510452909847.png\",\"car\":\"Ford\",\"confidence\":\"0.1005\"},{\"image\":\"http://kodu.ut.ee/~matukopp/ftptest/video-frame-1510452910472.png\",\"car\":\"MINI\",\"confidence\":\"0.1016\"},{\"image\":\"http://kodu.ut.ee/~matukopp/ftptest/video-frame-1510452911062.png\",\"car\":\"Ford\",\"confidence\":\"0.0804\"},{\"image\":\"http://kodu.ut.ee/~matukopp/ftptest/video-frame-1510452911712.png\",\"car\":\"Mitsubishi\",\"confidence\":\"0.179\"},{\"image\":\"http://kodu.ut.ee/~matukopp/ftptest/video-frame-1510452912305.png\",\"car\":\"MINI\",\"confidence\":\"0.1099\"},{\"image\":\"http://kodu.ut.ee/~matukopp/ftptest/video-frame-1510452912943.png\",\"car\":\"Subaru\",\"confidence\":\"0.3124\"},{\"image\":\"http://kodu.ut.ee/~matukopp/ftptest/video-frame-1510452913543.png\",\"car\":\"Honda\",\"confidence\":\"0.1123\"},{\"image\":\"http://kodu.ut.ee/~matukopp/ftptest/video-frame-1510452914114.png\",\"car\":\"MINI\",\"confidence\":\"0.191\"},{\"image\":\"http://kodu.ut.ee/~matukopp/ftptest/video-frame-1510452914706.png\",\"car\":\"BMW\",\"confidence\":\"0.1589\"},{\"image\":\"http://kodu.ut.ee/~matukopp/ftptest/video-frame-1510452915314.png\",\"car\":\"MINI\",\"confidence\":\"0.1994\"},{\"image\":\"http://kodu.ut.ee/~matukopp/ftptest/video-frame-1510452915869.png\",\"car\":\"MINI\",\"confidence\":\"0.4516\"}]}";

        // TODO this should be in service
        // TODO stop with some confidence
        if (file != null) {
            File convFile = new File(filepath + file.getOriginalFilename());
            try {
                //System.out.println(detectionService.encode(file.getBytes()));
                convFile.createNewFile();
                FileOutputStream fos = new FileOutputStream(convFile);
                fos.write(file.getBytes());
                fos.close();
                System.out.println("Your File Name is : " + convFile.getName());
                // TODO dont save and send directly
                // get image frames;
                System.out.println("taking out frames");
                ArrayList<String> files = FrameExtractor.extract("C:\\hackaton\\" + convFile.getName());
                ArrayList<ArrayList<String>> results = new ArrayList();
                for (String f : files) {
                    System.out.println(f);
                    String imgLoc = detectionService.uploadToDFTP(f);
                    System.out.println("uploaded to " + imgLoc);
                    String resp = detectionService.getimage(imgLoc, "application/json");
                    //System.out.println("response!");
                    System.out.println(resp);
                    ArrayList parsed = null;
                    try {
                        parsed = parseJson(resp);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (parsed == null) {

                    } else {
                        // find only first car
                        parsed.add(imgLoc);
                        results.add(parsed);
                        // System.out.println("P" + parsed);
                        // System.out.println("R " + results);
                        // break;
                    }
                }

                System.out.println("make return json");
                // System.out.println(results);
                JSONObject returnJson = new JSONObject();
                JSONArray arr = new JSONArray();
                for (ArrayList<String> t : results) {
                    JSONObject tmp = new JSONObject();
                    try {
                        tmp.put("car", t.get(0));
                        tmp.put("confidence", t.get(1));
                        tmp.put("image", t.get(2));

                        arr.put(tmp);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                try {
                    returnJson.put("results", arr);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //String resp = detectionService.getimage(imgLoc, "application/json");
                System.out.println("returning");
                System.out.println(returnJson.toString());
                return returnJson.toString();
            } catch (FileNotFoundException e) {
                System.out.println(e);
            } catch (IOException e) {
                System.out.println(e);
            }
        }
        return "500";
    }

    private ArrayList<String> parseJson(String in) throws JSONException {
        JSONObject obj = new JSONObject(in);
        JSONObject objects;
        try {
            objects = obj.getJSONArray("objects").getJSONObject(0);
        } catch (JSONException e) {
            System.out.println("no cars detected in frame");
            return null;
        }
        JSONObject make = objects
                .getJSONObject("vehicleAnnotation")
                .getJSONObject("attributes")
                .getJSONObject("system")
                .getJSONObject("make");
        String name = make.getString("name");
        String confidence = make.getString("confidence");

        ArrayList<String> out = new ArrayList<>();
        out.add(name);
        out.add(confidence);
        return out;
    }
}

package com.ut.hackaton.demo;

import com.ut.hackaton.demo.helpers.FTPService;
import com.ut.hackaton.demo.helpers.FrameExtractor;
import org.bytedeco.javacv.FrameGrabber;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

@SpringBootApplication
public class DemoApplication {

	// THANK YOU!! https://github.com/KanchanaThiliniAbeyrathna/File-Upload-Angular-2-and-Spring-MVC
	public static void main(String[] args) throws IOException, FrameGrabber.Exception {
		SpringApplication.run(DemoApplication.class, args);
	}
}
